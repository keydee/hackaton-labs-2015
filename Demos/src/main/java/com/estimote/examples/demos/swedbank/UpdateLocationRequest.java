package com.estimote.examples.demos.swedbank;

import com.estimote.examples.demos.swedbank.BeaconDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wik on 6/11/2015.
 */
public class UpdateLocationRequest {
    private String pid;
    private List<BeaconDetails> beacons = new ArrayList<>();

    public UpdateLocationRequest() {
    }

    public UpdateLocationRequest(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<BeaconDetails> getBeacons() {
        return beacons;
    }
}
