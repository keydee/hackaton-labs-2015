package com.estimote.examples.demos.swedbank;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.estimote.examples.demos.BeaconBase;
import com.estimote.examples.demos.HttpGetTask;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by p998dln on 2015.06.11.
 */
public class BeaconMonitoringService extends IntentService {

    private static final String URL = "https://labspositioning.herokuapp.com/api/beacons/list";
    private static final String TAG = BeaconMonitoringService.class.getSimpleName();
    private BeaconManager beaconManager = new BeaconManager(this);
    private NotificationManager notificationManager;
    List<Region> regions = new LinkedList<Region>();

    public BeaconMonitoringService(String name) {
        super(name);
    }

    public BeaconMonitoringService(){
        super("name");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.wtf("AAA", intent.toString());

        getBeaconsAllBeacons();

        // Default values are 5s of scanning and 25s of waiting time to save CPU cycles.
        // In order for this demo to be more responsive and immediate we lower down those values.
        beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 0);
        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> beacons) {
                Log.wtf("AAA entered region", region.toString() + beacons.toString().toString());
/*                UpdateLocationAsyncTask task = new UpdateLocationAsyncTask();
                task.setPrefs(settings);
                beaconManager.
                task.execute(visibleBeacons.toArray(new Beacon[visibleBeacons.size()]));*/
            }

            @Override
            public void onExitedRegion(Region region) {
                Log.wtf("AAA left region", region.toString());
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    for(Region region:regions){
                        beaconManager.startMonitoring(region);
                        Log.wtf("AAA Starting to monitor", region.toString());
                    }
                } catch (RemoteException e) {
                    Log.d(TAG, "Error while starting monitoring");
                }
            }
        });
    }

    private void getBeaconsAllBeacons() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        try {
            response = httpclient.execute(new HttpGet(URL));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                BeaconBase[] beacons = new Gson().fromJson(out.toString(), BeaconBase[].class);
                for (BeaconBase beacon : beacons) {
                    regions.add(new Region(beacon.name, beacon.uuid, beacon.major, beacon.minor));
                }
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            //TODO Handle problems..
        }
    }
}