package com.estimote.examples.demos;

/**
 * Created by p998dln on 2015.06.11.
 */
public interface AsyncResponse {
    void processFinish(String output);
}