package com.estimote.examples.demos.swedbank;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.estimote.examples.demos.AllDemosActivity;
import com.estimote.sdk.Beacon;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wik on 6/11/2015.
 */
public class UpdateLocationAsyncTask extends AsyncTask<Beacon, Void, UpdateLocationResponse> {

    private static final String TAG = UpdateLocationAsyncTask.class.getSimpleName();
    private static SharedPreferences prefs;

    @Override
    protected UpdateLocationResponse doInBackground(Beacon... beacons) {
        if (beacons.length > 0) {
            UpdateLocationResponse response = updateLocation(Arrays.asList(beacons));
            Log.i(TAG, "RESPONSE2: " + response.toString());
            return response;
        }
        return null;
    }

    @Override
    protected void onPostExecute(UpdateLocationResponse updateLocationResponse) {
        super.onPostExecute(updateLocationResponse);
    }

    private UpdateLocationResponse updateLocation(final List<Beacon> beacons) {
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        UpdateLocationResponse responseObj = null;
        try {
            HttpPost post = createHttpPost(beacons);
            HttpResponse response = client.execute(post);
            if (response != null) {
                InputStream in = response.getEntity().getContent();
                responseObj = new Gson().fromJson(new InputStreamReader(in), UpdateLocationResponse.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Cannot Establish Connection");
        }
        return responseObj;
    }

    private static HttpPost createHttpPost(final List<Beacon> beacons) throws IOException {
        HttpPost post = new HttpPost("https://labspositioning.herokuapp.com/api/locations/update");
        UpdateLocationRequest request = new UpdateLocationRequest(prefs.getString(AllDemosActivity.PID, null));
        for (Beacon beacon : beacons) {
            request.getBeacons().add(new BeaconDetails(beacon));
        }
        StringEntity se = new StringEntity(new Gson().toJson(request));
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        post.setEntity(se);
        Log.i(TAG, "REQUEST: " + getStringFromInputStream(post.getEntity().getContent()));
        return post;
    }

    private static String getStringFromInputStream(InputStream inputStream) {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            total.append(e.getMessage());
        }
        return total.toString();
    }

    public static void setPrefs(SharedPreferences prefs) {
        UpdateLocationAsyncTask.prefs = prefs;
    }
}
