package com.estimote.examples.demos;

import java.util.List;

/**
 * Created by p998dln on 2015.06.11.
 */
/*public class Beacons {
    public List<Beacon> beacons;
}*/

public class BeaconBase {
    public Integer major;
    public Integer minor;
    public String uuid;
    public String name;
};