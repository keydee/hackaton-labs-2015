package com.estimote.examples.demos.swedbank;

import java.util.Map;

/**
 * Created by Wik on 6/11/2015.
 */
public class UpdateLocationResponse {
    Map<String, Location> locations;

    @Override
    public String toString() {
        return "UpdateLocationResponse{" +
                "locations=" + locations +
                '}';
    }
}
