package com.estimote.examples.demos.swedbank;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

/**
 * Created by Wik on 6/11/2015.
 */
public class BeaconDetails {
    int major;
    int minor;
    int power;
    int rssi;
    double distance;

    public BeaconDetails() {
    }

    public BeaconDetails(Beacon beacon) {
        this.major = beacon.getMajor();
        this.minor = beacon.getMinor();
        this.power = beacon.getMeasuredPower();
        this.rssi = beacon.getRssi();
        this.distance = Utils.computeAccuracy(beacon);
    }
}
