package com.estimote.examples.demos.swedbank;

/**
 * Created by Wik on 6/11/2015.
 */
public class Location {
    private double x;
    private double y;
    private String message;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Location{" +
                "x=" + x +
                ", y=" + y +
                ", message='" + message + '\'' +
                '}';
    }
}
